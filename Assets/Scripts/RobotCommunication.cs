using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System.Net.Sockets;
using TwinCAT.Ads;

public class RobotCommunication : MonoBehaviour
{
    private TcAdsClient adsClient;

    private int CoffeValue_int;
    private string CoffeValue_str;

    public Button startButton;
    public int giftIndex = 0;
    public int indexLimit = 22;

    // Use this for initialization
    void Start()
    {
        Connect();
        startButton.onClick.AddListener(SendGiftIndexData);
    }


    void Connect()
    {
        try
        {
            adsClient = new TcAdsClient();
            adsClient.Connect("5.31.28.120.1.1", 801);

            CoffeValue_int = adsClient.CreateVariableHandle(".Robot_String");
            //ReadSignal_int = adsClient.CreateVariableHandle(".Send_Robot_String");

            print("Connected!");
        }
        catch (SocketException err)
        {
            print("Connection Error!" + err);
        }
    }

    public void SendGiftIndexData()
    {
        CoffeValue_str = (giftIndex + 1).ToString();
        SendData_PLC();
        Invoke("reset", 1f);
        UpdateIndex();
    }

    public void UpdateIndex()
    {
        giftIndex += 1;
        if (giftIndex >= 22)
        {
            giftIndex = 0;
        }
    }

    public void reset()
    {
        CoffeValue_str = "0";
        SendData_PLC();
    }

    public void SendData_PLC()
    {
        AdsStream SendData = new AdsStream(30);
        AdsBinaryWriter Data_write = new AdsBinaryWriter(SendData);
        Data_write.WritePlcString(CoffeValue_str, CoffeValue_str.Length);
        adsClient.Write(CoffeValue_int, SendData);
    }

    //public void ReadData_PLC()
    //{
    //    AdsStream adsStream = new AdsStream(30);
    //    AdsBinaryReader reader = new AdsBinaryReader(adsStream);
    //    adsClient.Read(ReadSignal_int, adsStream);
    //    ReadSignal_Str = reader.ReadPlcString(30);

    //    print(ReadSignal_Str);
    //}

}
