﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Data;
using System.Threading;
using TwinCAT.Ads;

public class PLCsparkslab : MonoBehaviour
{

    private TcAdsClient adsClient;
    private bool serverStarted;

    string flag_go_str = "0";
    string flag_move_str = "0";
    string flag_estop_str = "8";

    int Write_To_PLC_flag_move;
    int Write_To_PLC_flag_estop;
    int Write_To_PLC_flag_go;
    int Write_To_PLC;

    public Text m_MyText_;
    public Button go_;

    [Header("Editable Values")]

    public string robotIP = "5.31.28.120.1.1";
    public int incrementLimit = 22;
    public int Actual_Value_int = 0;
    public string Actual_Value = "0";
    public float flag1Delay = 1;
    public float moveFlagDelay = 1;

    [Header("Coroutine Checkers")]
    public bool coroutineGoStarted = false;
    public bool coroutineMoveStarted = false;

    void Start()
    {
        coroutineGoStarted = false;
        coroutineMoveStarted = false;

        flag1Delay = ConfigHandler.GetInstance().GetFloatValue("delayTimeFlag1");
        moveFlagDelay = ConfigHandler.GetInstance().GetFloatValue("moveFlagDelay");

        Actual_Value_int = ConfigHandler.GetInstance().GetValue("currentIndex");
        Actual_Value = Actual_Value_int.ToString();

        robotIP = ConfigHandler.GetInstance().GetStringValue("robotIP");
        incrementLimit = ConfigHandler.GetInstance().GetValue("maxIndex");
        Connection();

        WriteToPLCflag_0();

        Actual_Value_int = 0;
        Actual_Value = Actual_Value_int.ToString(); // int to string conversion
        WritePLC();

        Button go_btn = go_.GetComponent<Button>();
        go_.onClick.AddListener(go__PROCESS);
    }

    public void Connection()
    {
        try
        {
            adsClient = new TcAdsClient();
            adsClient.Connect(robotIP, 801);

            Write_To_PLC = adsClient.CreateVariableHandle(".Value_unity");
            Write_To_PLC_flag_go = adsClient.CreateVariableHandle(".flag_go");
            Write_To_PLC_flag_move = adsClient.CreateVariableHandle(".flag_move");
            Write_To_PLC_flag_estop = adsClient.CreateVariableHandle(".flag_eStop");

            ///Buttons

            serverStarted = true;

            print("PLC CONNECTED!!");
            m_MyText_.text = ("PLC CONNECTED!!");
        }
        catch
        {
            print("PLC NOT CONNECTED!!");
            m_MyText_.text = ("PLC NOT CONNECTED!!");
            serverStarted = false;
        }
    }

    public void go__PROCESS()
    {
        if (Actual_Value_int == incrementLimit)
        {
            Actual_Value_int = 0;
            Actual_Value = Actual_Value_int.ToString(); // int to string conversion
            WritePLC();
        }

        Actual_Value_int += 1;
        Actual_Value = Actual_Value_int.ToString(); // int to string conversion
        ConfigHandler.GetInstance().ChangeLine(Actual_Value, "currentIndex");
        WritePLC();

        if (!coroutineGoStarted)
            StartCoroutine(DelayedWriteToPLCFlagGO());
        Invoke("WriteToPLCflag_0", 5.0f);
    }
    public void WritePLC()
    {
        AdsStream Write_Stream = new AdsStream(30);
        AdsBinaryWriter Stream_wirte = new AdsBinaryWriter(Write_Stream);
        Stream_wirte.WritePlcString(Actual_Value, Actual_Value.Length);
        adsClient.Write(Write_To_PLC, Write_Stream);

        print("Gift!: " + Actual_Value);
    }

    public void SendClickerMessage(int p_value)
    {
        AdsStream Write_Stream = new AdsStream(30);
        AdsBinaryWriter Stream_wirte = new AdsBinaryWriter(Write_Stream);
        Stream_wirte.WritePlcString(p_value.ToString(), flag_go_str.Length);
        adsClient.Write(Write_To_PLC_flag_move, Write_Stream);

        if (!coroutineMoveStarted)
            StartCoroutine(DelayedWriteToPLCFlagMOVE());

        Debug.LogAssertion($"Send Clicker Value {p_value}");
    }

    public void SendEmergencyStopMessage(int p_value)
    {
        AdsStream Write_Stream = new AdsStream(30);
        AdsBinaryWriter Stream_wirte = new AdsBinaryWriter(Write_Stream);
        Stream_wirte.WritePlcString(p_value.ToString(), flag_estop_str.Length);
        adsClient.Write(Write_To_PLC_flag_estop, Write_Stream);

        Debug.LogAssertion($"Sent Emergency Clicker Value {p_value}");
    }

    public IEnumerator DelayedWriteToPLCFlagGO()
    {
        coroutineGoStarted = true;
        yield return new WaitForSeconds(flag1Delay);
        Debug.LogAssertion("Go Delay Complete");
        WriteToPLC_flag_go_1();
        coroutineGoStarted = false;
    }

    public IEnumerator DelayedWriteToPLCFlagMOVE()
    {
        coroutineMoveStarted = true;
        yield return new WaitForSeconds(moveFlagDelay);
        Debug.LogAssertion("Move Delay Complete");
        SendPLCStringMove(0);
        coroutineMoveStarted = false;
    }

    public void SendPLCStringMove(int p_value)
    {
        flag_move_str = "0";

        AdsStream Write_Stream = new AdsStream(30);
        AdsBinaryWriter Stream_wirte = new AdsBinaryWriter(Write_Stream);
        Stream_wirte.WritePlcString(flag_move_str, flag_go_str.Length);
        adsClient.Write(Write_To_PLC_flag_move, Write_Stream);
        Debug.LogAssertion($"Send Move Flag {p_value}");
    }

    public void WriteToPLC_flag_go_1()
    {
        flag_go_str = "1";

        AdsStream Write_Stream = new AdsStream(30);
        AdsBinaryWriter Stream_wirte = new AdsBinaryWriter(Write_Stream);
        Stream_wirte.WritePlcString(flag_go_str, flag_go_str.Length);
        adsClient.Write(Write_To_PLC_flag_go, Write_Stream);
        Debug.LogAssertion("Send Flag Go 1");
        print("GO!: " + flag_go_str);
    }

    public void WriteToPLCflag_0()
    {
        flag_go_str = "0";
        AdsStream Write_Stream = new AdsStream(30);
        AdsBinaryWriter Stream_wirte = new AdsBinaryWriter(Write_Stream);
        Stream_wirte.WritePlcString(flag_go_str, flag_go_str.Length);
        adsClient.Write(Write_To_PLC_flag_go, Write_Stream);
        Debug.LogAssertion("Send Flag Go 2");

        print("GO!: " + flag_go_str);
    }

}