using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ConfigHandler : MonoBehaviour
{
    private static ConfigHandler _instance;

    [Header("File")]
    [SerializeField] string m_filePath = "/config.txt";

    public static ConfigHandler GetInstance()
    {
        return _instance;
    }

    void Awake()
    {
        if (_instance == null) { _instance = this; }
        else
        {
            Destroy(this);
        }
    }
    
    public string GetStringValue(string p_id)
    {
        string path = Application.streamingAssetsPath + m_filePath;

        StreamReader reader = new StreamReader(path);

        int m_count = File.ReadAllLines(path).Length;

        string[] linesRead = File.ReadAllLines(path);

        foreach (var line in linesRead)
        {
            line.Trim(' ');
            var lineSplit = line.Split(':');

            if (lineSplit[0].Equals(p_id) || lineSplit[0].Contains(p_id))
            {
                return lineSplit[1];
            }
        }
        reader.Close();
        return "";
    }

    public void ChangeLine(string newText, string p_dataID)
    {
        string path = Application.streamingAssetsPath + m_filePath;
        string[] arrLine = File.ReadAllLines(path);
     
        for (int lineIndex = 0; lineIndex < arrLine.Length; lineIndex++)
        {
            var lineSplit = arrLine[lineIndex].Split(':');
            if (lineSplit[0].Equals(p_dataID) || lineSplit[0].Contains(p_dataID))
            {
                arrLine[lineIndex] = $"{p_dataID}:{newText}";
            }
        }
        File.WriteAllLines(path, arrLine);
    }

    public int GetValue(string p_id)
    {
        string path = Application.streamingAssetsPath + m_filePath;

        StreamReader reader = new StreamReader(path);

        int m_count = File.ReadAllLines(path).Length;

        string[] linesRead = File.ReadAllLines(path);

        foreach (var line in linesRead)
        {
            line.Trim(' ');
            var lineSplit = line.Split(':');

            if (lineSplit[0].Equals(p_id) || lineSplit[0].Contains(p_id))
            {
                return int.Parse(lineSplit[1]);
            }
        }
        reader.Close();
        return -1;
    }

    public float GetFloatValue(string p_id)
    {
        string path = Application.streamingAssetsPath + m_filePath;

        StreamReader reader = new StreamReader(path);

        int m_count = File.ReadAllLines(path).Length;

        string[] linesRead = File.ReadAllLines(path);

        foreach (var line in linesRead)
        {
            line.Trim(' ');
            var lineSplit = line.Split(':');

            if (lineSplit[0].Equals(p_id) || lineSplit[0].Contains(p_id))
            {
                return float.Parse(lineSplit[1]);
            }
        }
        reader.Close();
        return -1;
    }

}