using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    private static UIManager instance;

    public static UIManager GetInstance()
    {
        return instance;
    }

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }

    public GameObject startScreen;
    public GameObject loadingScreen;
    public GameObject endScreen;

    public GameObject debugUI;
    public GameObject clickHereUI;
    public bool isLoading = false;
    bool debugModeOn = false;
    [Header("Editable Values")]
    public int robotTimeDelayTablet = 10;
    public int robotTimeDelayClicker = 10;

    public int endScreenTime = 10;

    void Start()
    {
        debugModeOn = false;
        debugUI.SetActive(false);
        robotTimeDelayTablet = ConfigHandler.GetInstance().GetValue("tabLoadingScreenTime");
        robotTimeDelayClicker = ConfigHandler.GetInstance().GetValue("clickerLoadingScreenTime");
        endScreenTime = ConfigHandler.GetInstance().GetValue("endScreenTime");
        Reset();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            debugModeOn = !debugModeOn;
            debugUI.SetActive(debugModeOn);
        }
    }

    public void InitLoading(bool p_srcTablet)
    {
        if (!isLoading)
            StartCoroutine(StartLoading(p_srcTablet));
    }

    public void Reset()
    {
        isLoading = false;
        startScreen.SetActive(true);
        clickHereUI.SetActive(true);

        loadingScreen.SetActive(false);
        endScreen.SetActive(false);
    }

    public IEnumerator StartLoading(bool p_isTabletSrc)
    {
        int robotTimeDelay = p_isTabletSrc ? robotTimeDelayTablet : robotTimeDelayClicker;
        isLoading = true;
        clickHereUI.SetActive(false);
        loadingScreen.SetActive(true);
        yield return new WaitForSeconds(robotTimeDelay);
        loadingScreen.SetActive(false);
        endScreen.SetActive(true);
        yield return new WaitForSeconds(endScreenTime);
        Reset();
    }

}