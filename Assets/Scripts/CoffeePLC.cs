﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using UnityEngine.UI;
using UnityEngine.Video;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System;
using TwinCAT.Ads;


public class CoffeePLC : MonoBehaviour {

    private TcAdsClient adsClient;

    private int ReadSignal_int;
    private string ReadSignal_Str;

    private int CoffeValue_int;
    private string CoffeValue_str;



    public Button CoffeeBtn1;
    public Button CoffeeBtn2;
    public Button CoffeeBtn3;
    public Button CoffeeBtn4;
    public Button CoffeeBtn5;
    public Button CoffeeBtn6;



    // Use this for initialization
    void Start ()
    {
        Connect();
        CoffeeBtn1.onClick.AddListener(Coffee_1);
        CoffeeBtn2.onClick.AddListener(Coffee_2);
        CoffeeBtn3.onClick.AddListener(Coffee_3);
        CoffeeBtn4.onClick.AddListener(Coffee_4);
        CoffeeBtn5.onClick.AddListener(Coffee_5);
        CoffeeBtn6.onClick.AddListener(Coffee_6);
    }


    void Connect()
    {
        try
        {
            adsClient = new TcAdsClient();
            adsClient.Connect("5.31.28.120.1.1", 801);

            CoffeValue_int = adsClient.CreateVariableHandle(".Robot_String");
            ReadSignal_int = adsClient.CreateVariableHandle(".Send_Robot_String");

            print("Connected!");
        }       
        catch (SocketException err)
        {
            print("Connection Error!" + err);
        }
    }

    // Update is called once per frame
    void Update () {
        
        ReadData_PLC();
    }

    public void Coffee_1()
    {
        CoffeValue_str = "1";
        SendData_PLC();
        Invoke("reset", 1f);
    }

    public void Coffee_2()
    {
        CoffeValue_str = "2";
        SendData_PLC();
        Invoke("reset", 1f);
    }
    public void Coffee_3()
    {
        CoffeValue_str = "3";
        SendData_PLC();
        Invoke("reset", 1f);
    }

    public void Coffee_4()
    {
        CoffeValue_str = "4";
        SendData_PLC();
        Invoke("reset", 1f);
    }

    public void Coffee_5()
    {
        CoffeValue_str = "5";
        SendData_PLC();
        Invoke("reset", 1f);
    }

    public void Coffee_6()
    {
        CoffeValue_str = "6";
        SendData_PLC();
        Invoke("reset", 1f);
    }

    public void reset()
    {
        CoffeValue_str = "0";
        SendData_PLC();
    }

    public void SendData_PLC()
    {
        AdsStream SendData = new AdsStream(30);
        AdsBinaryWriter Data_write = new AdsBinaryWriter(SendData);
        Data_write.WritePlcString(CoffeValue_str, CoffeValue_str.Length);
        adsClient.Write(CoffeValue_int, SendData);
    }

    public void ReadData_PLC()
    {
        AdsStream adsStream = new AdsStream(30);
        AdsBinaryReader reader = new AdsBinaryReader(adsStream);
        adsClient.Read(ReadSignal_int, adsStream);
        ReadSignal_Str = reader.ReadPlcString(30);

        print(ReadSignal_Str);
    }


}
