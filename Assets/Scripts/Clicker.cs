using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clicker : MonoBehaviour
{
    [SerializeField] PLCsparkslab m_plcSparkslab;
    [Header("Up and Down Keys")]
    [SerializeField] int m_clickValueUP;
    [SerializeField] int m_clickValueDown;
    [Header("Emergency Stop")]
    [SerializeField] int m_clickValueStop;
    [SerializeField] int m_clickValueStart;
    [SerializeField] int m_stopTime = 5;
    private bool isStopped = false;

    void Start()
    {
        m_clickValueUP = ConfigHandler.GetInstance().GetValue("clickerMessageUP");
        m_clickValueDown = ConfigHandler.GetInstance().GetValue("clickerMessageDown");
        m_clickValueStop = ConfigHandler.GetInstance().GetValue("clickerMessageStop");
        m_clickValueStart = ConfigHandler.GetInstance().GetValue("clickerMessageStart");
        m_stopTime = ConfigHandler.GetInstance().GetValue("emergencyStopTime");

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.PageUp) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            UIManager.GetInstance().InitLoading(false);
            m_plcSparkslab.SendClickerMessage(m_clickValueUP);
        }
        if (Input.GetKeyDown(KeyCode.PageDown) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            UIManager.GetInstance().InitLoading(false);
            m_plcSparkslab.SendClickerMessage(m_clickValueDown);
        }
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (!isStopped)
                StartCoroutine(EmergencyStop());
        }
    }

    public IEnumerator EmergencyStop()
    {
        isStopped = true;
        Debug.LogAssertion("EMERGENCY STOP!");
        Debug.Log($"Sent {m_clickValueStop}");
        m_plcSparkslab.SendEmergencyStopMessage(m_clickValueStop);
        yield return new WaitForSeconds(m_stopTime);
        m_plcSparkslab.SendEmergencyStopMessage(m_clickValueStart);
        Debug.LogAssertion("Robot Started");
        Debug.Log($"Sent {m_clickValueStart}");
        isStopped = false;
    }

}