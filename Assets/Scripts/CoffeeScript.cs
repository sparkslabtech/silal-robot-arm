﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEditor;
//using System.IO;
//using UnityEngine.UI;
//using UnityEngine.Video;
//using System.Linq;
//using System.Text;
//using System.Net;
//using System.Net.Sockets;
//using System.Threading;
//using System;


//public class CoffeeScript : MonoBehaviour {

//    //UDP Connection!
//    private string IP;  // define in init
//    int port;  // define in init
//    IPEndPoint remoteEndPoint;
//    UdpClient client;

//    public int CoffeValue = 0;
//    public Button CoffeeBtn1;
//    public Button CoffeeBtn2;
//    public Button CoffeeBtn3;
//    public Button CoffeeBtn4;


//    public string allReceivedUDPPackets = ""; // clean up this from time to time!


//    public void RobotConnect_UDP()
//    {
//        try
//        {
//            IP = "172.31.1.147";
//            port = 54600;
//            remoteEndPoint = new IPEndPoint(IPAddress.Parse(IP), port);
//            client = new UdpClient();
//            print("Sending to " + IP + " : " + port);
//            print("Connected!");

//            Byte[] receiveBytes = client.Receive(ref RemoteIpEndPoint);
//            allReceivedUDPPackets = Encoding.ASCII.GetString(receiveBytes);

//        }
//        catch (SocketException err)
//        {
//            print("Connection Error!" + err);
//        }
//    }

//    // Use this for initialization
//    void Start () {
//        RobotConnect_UDP();
//        CoffeeBtn1.onClick.AddListener(Coffee_1);
//        CoffeeBtn2.onClick.AddListener(Coffee_2);
//        CoffeeBtn3.onClick.AddListener(Coffee_3);
//        CoffeeBtn4.onClick.AddListener(Coffee_4);
//    }
	
//	// Update is called once per frame
//	void Update () {

//        SendData_UDP();
//        Print(allReceivedUDPPackets);
//    }

//    public void Coffee_1()
//    {
//        CoffeValue = 1;
//        Invoke("reset", 1f);
//    }

//    public void Coffee_2()
//    {
//        CoffeValue = 2;
//        Invoke("reset", 1f);
//    }
//    public void Coffee_3()
//    {
//        CoffeValue = 3;
//        Invoke("reset", 1f);
//    }

//    public void Coffee_4()
//    {
//        CoffeValue = 4;
//        Invoke("reset", 1f);
//    }

//    public void SendData_UDP()
//    {
//        string toSend1 = ("<xml><protec><type_>" + CoffeValue + "</type_></protec></xml>");
//        byte[] data1 = Encoding.UTF8.GetBytes(toSend1);
//        client.Send(data1, data1.Length, remoteEndPoint);
//        print(CoffeValue);        
//    }

//    public void reset()
//    {
//        CoffeValue = 0;
//    }

//}
